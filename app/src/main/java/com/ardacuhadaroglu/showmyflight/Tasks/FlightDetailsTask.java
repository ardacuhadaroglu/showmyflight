package com.ardacuhadaroglu.showmyflight.Tasks;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ardacuhadaroglu on 18/05/16.
 */
public class FlightDetailsTask extends AsyncTask<Void, Void, JSONObject> {

    public AsyncResponse delegate = null;

    private String flightNumber;

    public FlightDetailsTask(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    @Override
    protected JSONObject doInBackground(Void... params) {

        java.net.URL url;
        HttpURLConnection connection = null;
//            String urlParameters = "email="+mEmail+"&password="+mPassword;
        //String urlParameters = "email=trial02&password=demo1234demo";
        String flightNumber = "7224";
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("flight_number", this.flightNumber);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            //Create connection
            url = new URL(WebServiceManager.URL+ WebServiceManager.FLIGHT_DETAILS);
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/json");

            connection.setRequestProperty("Content-Length", "" + jsonObject.toString().length());
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches (false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream (
                    connection.getOutputStream ());
            wr.writeBytes(jsonObject.toString());
            wr.flush();
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();



            JSONObject object = new JSONObject(response.toString());

            return object;

        } catch (Exception e) {

            e.printStackTrace();
            return null;

        } finally {

            if(connection != null) {
                connection.disconnect();
            }
        }

    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        delegate.processFinished(jsonObject);
    }
}
