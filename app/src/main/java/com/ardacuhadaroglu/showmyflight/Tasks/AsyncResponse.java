package com.ardacuhadaroglu.showmyflight.Tasks;

import org.json.JSONObject;

/**
 * Created by ardacuhadaroglu on 18/05/16.
 */
public interface AsyncResponse {
    void processFinished(JSONObject resultJSON);

    void processFinished(String resultString);
}
