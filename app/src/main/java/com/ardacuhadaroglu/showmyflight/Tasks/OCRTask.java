package com.ardacuhadaroglu.showmyflight.Tasks;

import android.os.AsyncTask;

import com.ardacuhadaroglu.showmyflight.Apis.OCRServiceAPI;

import java.io.File;

/**
 * Created by ardacuhadaroglu on 19/05/16.
 */
public class OCRTask extends AsyncTask<Void, Void, String> {

    public AsyncResponse delegate = null;

    private File photoFile;

    public OCRTask(File photoFile) {
        this.photoFile = photoFile;
    }

    @Override
    protected String doInBackground(Void... params) {
        final OCRServiceAPI apiClient = new OCRServiceAPI("5BnBFH95Tu");
        apiClient.convertToText("tr", photoFile.getAbsolutePath());

        if (apiClient.getResponseCode() == 200) { //success response code
            return apiClient.getResponseText();
        } else
            return null;

    }

    @Override
    protected void onPostExecute(String responseText) {
        delegate.processFinished(responseText);
    }
}
