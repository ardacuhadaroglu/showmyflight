package com.ardacuhadaroglu.showmyflight;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ardacuhadaroglu.showmyflight.Tasks.AsyncResponse;
import com.ardacuhadaroglu.showmyflight.Tasks.FlightDetailsTask;
import com.ardacuhadaroglu.showmyflight.Tasks.OCRTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyFlightActivity extends AppCompatActivity implements AsyncResponse, View.OnClickListener{

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private Button showFlightButton;
    private Button scanWithCameraButton;
    private LinearLayout detailsLinearLayout;
    private LinearLayout flightNumberLinearLayout;
    private EditText flightNumberEditText;

    private File photoFile = null;
    String mCurrentPhotoPath;

    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_flight);

        if (getSupportActionBar() != null) {
            // enabling action bar app icon and behaving it as toggle button
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        showFlightButton = (Button) findViewById(R.id.showFlightButton);
        scanWithCameraButton = (Button) findViewById(R.id.scanWithCameraButton);
        showFlightButton.setOnClickListener(this);
        scanWithCameraButton.setOnClickListener(this);

        detailsLinearLayout = (LinearLayout) findViewById(R.id.detailsLinearLayout);
        flightNumberLinearLayout = (LinearLayout) findViewById(R.id.flightNumberLinearLayout);
        detailsLinearLayout.setVisibility(View.INVISIBLE);

        flightNumberEditText = (EditText) findViewById(R.id.flightNumberEditText);
        flightNumberEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                getFlightDetails(flightNumberEditText.getText().toString());
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        flightNumberEditText.requestFocus();
        flightNumberEditText.postDelayed(new Runnable() {

            @Override
            public void run() {
                InputMethodManager keyboard = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(flightNumberEditText, 0);
            }
        }, 200);

        Animation leftToRightAnimation = AnimationUtils.loadAnimation(this, R.anim.left_to_right);
        Animation rightToLeftAnimation = AnimationUtils.loadAnimation(this, R.anim.right_to_left);
        flightNumberLinearLayout.startAnimation(leftToRightAnimation);
        scanWithCameraButton.startAnimation(rightToLeftAnimation);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.showFlightButton) {

            getFlightDetails(flightNumberEditText.getText().toString());
        } else if (v.getId() == R.id.scanWithCameraButton) {
            dispatchTakePictureIntent();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            dialog = ProgressDialog.show( MyFlightActivity.this, getString(R.string.loading), getString(R.string.converting_text), true, false);

            OCRTask ocrTask = new OCRTask(photoFile);
            ocrTask.delegate = this;
            ocrTask.execute();

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private File createImageFile() throws IOException {

        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private void getFlightDetails(String flightNumber) {
        FlightDetailsTask flightDetailsTask = new FlightDetailsTask(flightNumber);
        flightDetailsTask.delegate = this;
        flightDetailsTask.execute();
    }

    @Override
    public void processFinished(final JSONObject resultJSON) {
        if (resultJSON != null) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    InputMethodManager keyboard = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    keyboard.hideSoftInputFromWindow(flightNumberEditText.getWindowToken(), 0);

                    LinearLayout detailsLinearLayout = (LinearLayout) findViewById(R.id.detailsLinearLayout);
                    TextView flightNoTextView = (TextView) findViewById(R.id.flightNoTextView);
                    TextView airlineTextView = (TextView) findViewById(R.id.airlineTextView);
                    TextView fromTextView = (TextView) findViewById(R.id.fromTextView);
                    TextView toTextView = (TextView) findViewById(R.id.toTextView);
                    TextView dateTextView = (TextView) findViewById(R.id.dateTextView);
                    TextView aircraftModelTextView = (TextView) findViewById(R.id.aircraftModelTextView);
                    TextView registrationTextView = (TextView) findViewById(R.id.registrationTextView);
                    TextView statusTextView = (TextView) findViewById(R.id.statusTextView);

                    try {
                        if (resultJSON != null) {
                            assert flightNoTextView != null;
                            flightNoTextView.setText(resultJSON.getString("flight_no"));
                            assert airlineTextView != null;
                            airlineTextView.setText(resultJSON.getString("airline"));
                            assert fromTextView != null;
                            fromTextView.setText(resultJSON.getString("from"));
                            assert toTextView != null;
                            toTextView.setText(resultJSON.getString("to"));
                            assert dateTextView != null;
                            dateTextView.setText(resultJSON.getString("date"));
                            assert aircraftModelTextView != null;
                            aircraftModelTextView.setText(resultJSON.getString("aircraft_model"));
                            assert registrationTextView != null;
                            registrationTextView.setText(resultJSON.getString("registration"));
                            assert statusTextView != null;
                            statusTextView.setText(resultJSON.getString("status"));

                            detailsLinearLayout.setVisibility(View.VISIBLE);
                            Animation downToUpAnimation = AnimationUtils.loadAnimation(MyFlightActivity.this, R.anim.down_to_up);
                            detailsLinearLayout.startAnimation(downToUpAnimation);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

        } else {
            Toast.makeText(MyFlightActivity.this, getString(R.string.error_get_flight), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void processFinished(String resultString) {

        dialog.dismiss();

        if (resultString != null) {
            String flightNumber = resultString.split("-")[1].trim();

            flightNumberEditText.setText(resultString);
            getFlightDetails(flightNumber);
        } else {
            Toast.makeText(MyFlightActivity.this, getString(R.string.error_ocr), Toast.LENGTH_LONG).show();
        }

    }
}
